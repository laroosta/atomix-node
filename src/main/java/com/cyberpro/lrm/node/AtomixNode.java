package com.cyberpro.lrm.node;

import com.cyberpro.lrm.server.TestSubject;

import io.atomix.cluster.Node;
import io.atomix.cluster.discovery.BootstrapDiscoveryProvider;
import io.atomix.core.Atomix;
import io.atomix.core.AtomixBuilder;
import io.atomix.core.map.DistributedMap;
import io.atomix.core.profile.Profile;

public class AtomixNode {
	public static void main(String[] args) {

		AtomixBuilder builder = Atomix.builder().withMemberId("node1").withAddress("localhost:8701");

		builder.withMembershipProvider(BootstrapDiscoveryProvider.builder()
				.withNodes(Node.builder().withId("genesis-member").withAddress("localhost:8700").build()).build());

		builder.addProfile(Profile.dataGrid());

		Atomix atomix = builder.build();
		atomix.start().join();

		System.out.println("node 1 joined the cluster");
		DistributedMap<Object, Object> map = atomix.getMap("my-map");

		TestSubject value = (TestSubject) map.get("foo");
		System.out.println(value.getName());

		TestSubject testSubject = new TestSubject("Lauren", "She is awesome!");

		map.replace("foo", value, testSubject);

	}
}
